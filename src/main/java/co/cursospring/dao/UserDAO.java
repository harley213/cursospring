package co.cursospring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.cursospring.model.User;

@Repository
public interface UserDAO extends JpaRepository<User, Long> {

	User findByName(String name);
}
