package co.cursospring.mapper;

import co.cursospring.dto.RoleDTO;
import co.cursospring.model.Role;

public class RoleMapper {

    public static Role dtoToEntity(RoleDTO roleDTO) {
        Role role = new Role();
        if (roleDTO != null) {
            if (roleDTO.getId() != null) {
                role.setId(roleDTO.getId());
            }

            role.setName(roleDTO.getName());
        }
        return role;
    }

    public static RoleDTO entityToDTO(Role role) {
        RoleDTO roleDTO = new RoleDTO();
        if (role != null) {
            roleDTO.setId(role.getId());
            roleDTO.setName(role.getName());
        }
        return roleDTO;
    }

}
