package co.cursospring.mapper;

import co.cursospring.dto.UserDTO;
import co.cursospring.model.Role;
import co.cursospring.model.User;

public class UserMapper {

	public static User dtoToEntity(UserDTO userDTO, Role role) {
		User user = new User();
		if (userDTO != null) {
			if (userDTO.getId() != null) {
				user.setId(userDTO.getId());
			}

			user.setEmail(userDTO.getEmail());
			user.setName(userDTO.getName());
			user.setPhone(userDTO.getPhone());
			user.setRole(role);

		}

		return user;
	}

	public static UserDTO entityToDTO(User user) {
		UserDTO userDTO = new UserDTO();
		if (user != null) {
			if (user.getId() != null) {
				userDTO.setId(user.getId());
			}

			userDTO.setEmail(user.getEmail());
			userDTO.setName(user.getName());
			userDTO.setPhone(user.getPhone());
			if (user.getRole() != null) {
				userDTO.setIdRole(user.getRole().getId());
			}

		}

		return userDTO;
	}

}
