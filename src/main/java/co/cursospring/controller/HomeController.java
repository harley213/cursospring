package co.cursospring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController extends BaseController {

    @RequestMapping("/error.do")
    public ModelAndView error() {
        return new ModelAndView("error");
    }

    @RequestMapping(value = { "/notFound.do", "/notFound" }, method = { RequestMethod.POST, RequestMethod.GET })
    public ModelAndView notFound() {
        return getNotFound();
    }

    @RequestMapping(value = { "/accessdenied", "/accessdenied.do" }, method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView getAccesoDenegado() {
        return new ModelAndView("accessdenied");
    }

    @RequestMapping("/")
    public ModelAndView welcome() {
        ModelAndView mav = new ModelAndView("welcome");
        mav.addObject("message", "prueba");
        return mav;
    }

}