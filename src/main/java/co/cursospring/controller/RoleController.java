package co.cursospring.controller;

import co.cursospring.dto.RoleDTO;
import co.cursospring.mapper.RoleMapper;
import co.cursospring.model.Role;
import co.cursospring.service.business.RoleService;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/role")
public class RoleController extends BaseController {

    private static final String MESSAGE = "message";

    @Autowired
    private RoleService roleService;

    @RequestMapping
    public ModelAndView index() {
        return new ModelAndView("role/index");
    }

    @RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
    public ModelAndView save(@Valid RoleDTO roleDTO) {

        ModelAndView mav = new ModelAndView("role/index");
        getLogger().info("roleDTO " + roleDTO);
        if (roleDTO != null) {
            Role role = RoleMapper.dtoToEntity(roleDTO);
            roleService.save(role);
            mav.addObject(MESSAGE, "se guardo el usuario");
        } else {
            mav.addObject(MESSAGE, "error al guardar el usuario");
        }

        return mav;
    }

    @RequestMapping(value = "/list", method = { RequestMethod.POST, RequestMethod.GET })
    public ModelAndView list() {

        ModelAndView mav = new ModelAndView("role/index");
        List<Role> roles = roleService.findAll();
        if (roles != null) {
            for (Role role : roles) {
                getLogger().info("role " + role.toString());
            }
            mav.addObject(MESSAGE, "se listaron los roles");
        } else {
            mav.addObject(MESSAGE, "no se encontraron roles");
        }

        return mav;
    }

}