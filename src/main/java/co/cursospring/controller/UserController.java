package co.cursospring.controller;

import co.cursospring.dto.UserDTO;
import co.cursospring.mapper.UserMapper;
import co.cursospring.model.User;
import co.cursospring.service.business.UserService;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/user")
public class UserController extends BaseController {

    private static final String MESSAGE = "message";

    @Autowired
    private UserService userService;

    @RequestMapping
    public ModelAndView index() {
        return new ModelAndView("user/index");
    }

    @RequestMapping(value = "/save", method = { RequestMethod.POST, RequestMethod.GET })
    public ModelAndView save(@Valid UserDTO userDTO) {

        ModelAndView mav = new ModelAndView("user/index");
        getLogger().info("userDTO " + userDTO);
        if (userDTO != null) {
       
            userService.save(userDTO);
            mav.addObject(MESSAGE, "se guardo el usuario");
        } else {
            mav.addObject(MESSAGE, "error al guardar el usuario");
        }

        return mav;
    }

    @RequestMapping(value = "/list", method = { RequestMethod.POST, RequestMethod.GET })
    public ModelAndView list() {

        ModelAndView mav = new ModelAndView("user/index");
        List<User> users = userService.findAll();
        if (users != null) {
            for (User user : users) {
                getLogger().info("user " + user.toString());
            }
            mav.addObject(MESSAGE, "se listaron los usuarios");
        } else {
            mav.addObject(MESSAGE, "no se encontraron usuarios");
        }

        return mav;
    }

}