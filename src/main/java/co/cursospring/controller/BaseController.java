package co.cursospring.controller;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;

public class BaseController {

    /**
     * Logger object that is instantiated depending on the class.
     */

    private Logger logger = LoggerFactory.getLogger(BaseController.class);

    public static final int MONTH_DATES = -1;

    public BaseController() {
        logger = LoggerFactory.getLogger(getClass());

    }

    private String getEntityKey(Object entity) {
        return StringUtils.uncapitalize(entity.getClass().getSimpleName());
    }

    protected void setBasicModel(ModelAndView mav, Object entity) {
        String entityKey = this.getEntityKey(entity);
        mav.addObject(entityKey, entity);
        mav.addObject("commandName", entityKey);

    }

    public ModelAndView getNotFound() {
        return new ModelAndView("notFound");
    }

    public String getProperty(String name) {
        String rst = "";
        try {
            Properties prop = PropertiesLoaderUtils.loadAllProperties("spring/spring.properties");
            rst = prop.getProperty(name);
        }
        catch (IOException e) {
            getLogger().error("error", e);
        }

        return rst;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

}
