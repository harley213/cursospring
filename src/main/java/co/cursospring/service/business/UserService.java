package co.cursospring.service.business;

import co.cursospring.dto.UserDTO;
import co.cursospring.model.User;

import java.util.List;

public interface UserService {

    User findById(Long id);

    void delete(Long id);

    User save(UserDTO userDTO);

    List<User> findAll();

    User findByName(String name);

}
