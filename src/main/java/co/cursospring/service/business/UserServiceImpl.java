package co.cursospring.service.business;

import co.cursospring.dao.RoleDAO;
import co.cursospring.dao.UserDAO;
import co.cursospring.dto.UserDTO;
import co.cursospring.mapper.UserMapper;
import co.cursospring.model.Role;
import co.cursospring.model.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private RoleService roleService;

	@Override
	public void delete(Long id) {
		userDAO.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public User findById(Long id) {

		return userDAO.findOne(id);
	}

	@Override
	public User save(UserDTO userDTO) {

		Role role = null;

		if (userDTO.getIdRole() != null) {
			role = roleService.findById(userDTO.getIdRole());
		}

		User user = UserMapper.dtoToEntity(userDTO, role);

		return userDAO.saveAndFlush(user);
	}

	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}

	@Override
	public User findByName(String name) {
		return userDAO.findByName(name);
	}

}
