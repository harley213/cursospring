package co.cursospring.service.business;

import co.cursospring.model.Role;

import java.util.List;

public interface RoleService {

    Role findById(Long id);

    void delete(Long id);

    Role save(Role role);

    List<Role> findAll();

    Role findByName(String name);

}
