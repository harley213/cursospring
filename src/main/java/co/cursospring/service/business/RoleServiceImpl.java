package co.cursospring.service.business;

import co.cursospring.dao.RoleDAO;
import co.cursospring.model.Role;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("roleService")
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    @Override
    public void delete(Long id) {
        roleDAO.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Role findById(Long id) {

        return roleDAO.findOne(id);
    }

    @Override
    public Role save(Role tipoProducto) {

        return roleDAO.saveAndFlush(tipoProducto);
    }

    @Override
    public List<Role> findAll() {
        return roleDAO.findAll();
    }

    @Override
    public Role findByName(String name) {
        return roleDAO.findByName(name);
    }

}
